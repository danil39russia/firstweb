window.addEventListener('DOMContentLoaded', () => {
    const menuNav = document.querySelector('.page-header__nav')
    console.log(menuNav)
    const menuButton = menuNav.querySelector('button')
    console.log(menuButton)
    const menuText = menuNav.querySelectorAll('a')
    console.log(menuText)
    
                   
    menuButton.addEventListener('click', event => {
        menuText.forEach(element => {
            if (element.style.display == 'flex'){
                element.style.display = 'none'
            } else {
                element.style.display = 'flex'
            }
        });
    })
    
    
    // menuButton.addEventListener('click', event => {
    //     if (menuText[0].style.display = 'none') {
    //         menuNav.querySelectorAll('a').forEach(element => {
    //             menuText.style.display = 'block'
    //             console.log('block')
    //         });
    //     }
    //     else {
    //         menuText.querySelectorAll('a').forEach(element => {
    //             menuText.style.display = 'none'
    //             console.log('none')
    //         });
    //     }
       
    // })
    const pageExamplesFilterButtons = document.querySelectorAll('.page-examples__filters button')
    console.log(pageExamplesFilterButtons)
    const examplesPhoto = document.querySelectorAll('.page-examples__photo')[0]
    const examplesAll = document.querySelectorAll('.page-examples__photo img')
    console.log(examplesPhoto)

    const examplesTurnkeyWebsite = [examplesPhoto.querySelector('#sk-strizhi'), 
                                    examplesPhoto.querySelector('#avito')]
    const examplesDevelopment = [examplesPhoto.querySelector('#watchman'), 
                                examplesPhoto.querySelector('#ermitazh'),
                                examplesPhoto.querySelector('#statistic')]
    const examplesDesign = [examplesPhoto.querySelector('#graphic')]          
    const examplesFilters = [examplesAll, examplesTurnkeyWebsite, examplesDevelopment, examplesDesign] 
    console.log(examplesFilters)
    // pageExamplesFilterButtons[0].style.cssText ='transition: all 0.5s ease; background-color:black; color: white;'
    for (let i = 0; i < pageExamplesFilterButtons.length; i++){ //ЧЕРЕЗ FOREACH НЕ ПОЛУЧИТСЯ
        pageExamplesFilterButtons[i].addEventListener('click', (event) => {
            for (let j = 0; j < pageExamplesFilterButtons.length; j++){
                pageExamplesFilterButtons[j].style.cssText ='transition: all 0.5s ease; background-color:white; color: black;'
                
            }
            pageExamplesFilterButtons[i].style.cssText ='transition: all 0.5s ease; background-color:black; color: white;'
            examplesAll.forEach(allPhoto => {
                allPhoto.style.display = 'none'
            })
            for(let k = 0; k < examplesFilters[i].length; k++){
                if (k%3 == 0){
                    examplesFilters[i][k].classList.remove('two-photo')
                    examplesFilters[i][k].classList.add('one-photo')
                    examplesFilters[i][k].style.display = 'block'
                // } else if ((k+1)%3 == 0) {
                //     examplesFilters[i][k].classList.remove('one-photo')
                //     examplesFilters[i][k].classList.add('two-photo')
                //     examplesFilters[i][k].style.display = 'inline-block'
                } else {
                    examplesFilters[i][k].classList.remove('one-photo')
                    examplesFilters[i][k].classList.add('two-photo')
                    examplesFilters[i][k].style.display = 'inline-block'
                }
                if (examplesFilters[i].length % 3 != 0){
                    let last = examplesFilters[i].length - 1
                    examplesFilters[i][last].classList.remove('two-photo')
                    examplesFilters[i][last].classList.add('one-photo')
                    examplesFilters[i][last].style.display = 'block'
                }
                // if (k%3 == 0){
                //     examplesFilters[i][k].style.display = 'block'
                //     examplesFilters[i][k].style.width = '100%'
                //     examplesFilters[i][k].style.marginLeft = '0'
                // }
                // else if ((k+1)%3 == 0) {
                //     examplesFilters[i][k].style.display = 'inline-block'
                //     examplesFilters[i][k].style.width = 'calc(50% - 20px)'
                //     examplesFilters[i][k].style.marginLeft = '40px'
                // }
                // else {
                //     examplesFilters[i][k].style.display = 'inline-block'
                //     examplesFilters[i][k].style.width = 'calc(50% - 20px)'
                //     examplesFilters[i][k].style.marginLeft = '0'
                // }
                // if (examplesFilters[i].length % 3 != 0){
                //     let last = examplesFilters[i].length - 1
                //     examplesFilters[i][last].style.display = 'block'
                //     examplesFilters[i][last].style.width = '100%'
                //     examplesFilters[i][last].style.marginLeft = '0'
                // }
            }
        })
    }
    pageExamplesFilterButtons[0].click();
    
    // pageExamplesFilterButtons.forEach((button) => {
    //     button.addEventListener('click', (event) => {
    //         pageExamplesFilterButtons.forEach((button) => {
    //             button.style.cssText ='transition: all 0.5s ease; background-color:white; color: black;'
    //         })
    //         event.target.style.cssText ='transition: all 0.5s ease; background-color:black; color: white;'
    //     })
    // })
    const pagePriceWorkButton = document.getElementById("page-price__type-work")
    const pagePriceWorkloadButton = document.getElementById("page-price__workload")
    const calculatePriceButton = document.getElementById("price-calculate")
    const calculatedPrice = document.getElementById("page-price__discussion")


    pagePriceWorkButton.addEventListener("change", () => {
        pagePriceWorkloadButton.disabled = (pagePriceWorkButton.value == "empty")
        // calculatePriceButton.disabled = (pagePriceWorkButton.value == "empty")
    })
    calculatePriceButton.addEventListener("click", () => {
        console.log(pagePriceWorkButton.value)
        if (pagePriceWorkButton.value == "empty"){
            alert("Сначала укажите вариант")
        } else {
            calculatedPrice.style.display = "block"
        }
    })
    const questionAndAnswerBlock = document.querySelectorAll(".page-question__block")
    console.log(questionAndAnswerBlock)
    questionAndAnswerBlock.forEach(element => {
        element.addEventListener("click", ()=> {
            let answerText = element.querySelector(".answer")
            let iconAnswerText = element.querySelector("img")
            if (answerText.style.maxHeight) {
                answerText.style.maxHeight = null;
                answerText.style.paddingBottom = "8px" // и строчка ниже - небольшой костыль, чтобы нормальные отступы были
                answerText.style.color = "white" 
                iconAnswerText.style.transform = ""
              } else {
                answerText.style.maxHeight = answerText.scrollHeight + "px";
                answerText.style.paddingBottom = "32px"
                answerText.style.color = "#181818"
                iconAnswerText.style.transform = "rotate(-225deg)"
              }
            // if (element.querySelector(".answer").style.display == "block"){
            //     // element.querySelector(".answer").style.transition = "all 2s ease"
            //     // element.querySelector(".answer").style.cssText = "max-height: 0px;"
            //     // element.querySelector(".answer").style.display = "none"
                
            //     element.querySelector("img").style.transform = ""
            //     element.querySelector("img").style.transition = "all 0.5s ease"
            // } else {
            //     // element.querySelector(".answer").style.transition = "all 2s ease"
            //     // element.querySelector(".answer").style.cssText = "max-height: 3000px;"
            //     // element.querySelector(".answer").style.display = "block"
            //     element.querySelector("img").style.transform = "rotate(-225deg)"
                
            //     element.querySelector("img").style.transition = "all 0.5s ease"

            // }
        })
    })
})